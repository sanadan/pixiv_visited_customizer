# Pixivの閲覧済み画像のサムネイルに枠をつけて強調するTampermonkeyスクリプト
![Pixiv visited customizer 使用例](docs/image.jpg)

　こんな感じで閲覧したことのある画像のサムネイルが強調されます。

　以下のリンクから、スクリプトをインストールできます。

https://gitlab.com/sanadan/pixiv_visited_customizer/-/raw/master/pixiv_visited_customizer.user.js
